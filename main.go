package main

import (
	"eos/internal/calculation"
	"eos/internal/converter"
	"fmt"
	"os"

	"go.uber.org/zap"
)

func main() {
	logger, err := zap.NewProduction()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	l := logger.Sugar()
	defer l.Sync()

	converter := converter.NewConverter()
	args, err := converter.ArgsToInts(os.Args)
	if err != nil {
		l.Fatalf("error occured: %v", err)
	}

	pairs, err := converter.ArgsToData(args)
	if err != nil {
		l.Fatalf("error occured: %v", err)
	}

	calc := calculation.NewCalculation(pairs)
	results := calc.Calculate()

	for _, v := range results {
		fmt.Println(v.Equal)
	}
}
