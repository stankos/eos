package models

type Data struct {
	Boxes  int
	Apples int
}

type Result struct {
	Equal string
}
