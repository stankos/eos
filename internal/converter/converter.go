package converter

import (
	"eos/internal/models"
	"fmt"
	"strconv"
)

type Converter struct{}

func NewConverter() *Converter {
	return &Converter{}
}

func (c *Converter) ArgsToData(args []int) (data []models.Data, err error) {
	cnt := 0
	for i := 0; i < len(args)/2; i++ {
		data = append(data, models.Data{
			Boxes:  args[cnt],
			Apples: args[cnt+1],
		})

		cnt = cnt + 2
	}

	return data, nil
}

func (c *Converter) ArgsToInts(args []string) (ret []int, err error) {
	// check min length
	if len(args) < 3 {
		return nil, fmt.Errorf("minimum 3 arguments are required")
	}

	// convert strings to ints
	for k, v := range args {
		// we ignore first two arguments
		if k > 1 {
			num, err := strconv.Atoi(v)
			if err != nil {
				return nil, fmt.Errorf("argument '%s' with index %d should be integer", v, k)
			}

			ret = append(ret, num)
		}
	}

	// check if arguments are pairs
	if !argsValid(ret) {
		return nil, fmt.Errorf("only pairs are allowed to be arguments")
	}

	return ret, nil
}

func argsValid(args []int) bool {
	a := len(args)
	b := len(args) / 2
	return a%b == 0
}
