package calculation

import (
	"eos/internal/models"
	"reflect"
)

type Calculation struct {
	pairs []models.Data
}

func NewCalculation(pairs []models.Data) *Calculation {
	return &Calculation{pairs: pairs}
}

func (c *Calculation) Calculate() (ret []models.Result) {
	for _, v := range c.pairs {
		res := "NO"
		if reflect.DeepEqual(workerOne(v), workerTwo(v)) {
			res = "YES"
		}

		ret = append(ret, models.Result{Equal: res})
	}

	return ret
}

func workerOne(data models.Data) []int {
	ret := make([]int, data.Boxes)
	t := data.Apples / data.Boxes
	for i := 0; i < t; i++ {
		ret[i] = data.Apples
	}

	return ret
}

func workerTwo(data models.Data) []int {
	ret := make([]int, data.Boxes)
	t := data.Boxes / data.Apples
	for i := 0; i < t; i++ {
		index := smallestIndex(ret)
		ret[index] = ret[index] + data.Apples
	}

	return ret
}

func smallestIndex(boxes []int) int {
	min := boxes[0]
	for _, v := range boxes {
		if v < min {
			min = v
		}
	}

	return min
}
